# README for fifconfig
## Metadata
Filename: README.md
Location: https://gitlab.com/bgstack15/fifconfig
Author: bgstack15
Startdate: 2022-03-15
Title: Flask-ifconfig, or Http client diagnostic webapp
Purpose: Replicate http://ifconfig.me
History:
Usage:
Reference: See References heading
Improve:
Dependencies: See Dependencies heading
Documentation:

## Upstream
[gitlab](https://gitlab.com/bgstack15/fifconfig)
[author's git](https://bgstack15.ddns.net/cgit/fifconfig)

## Features

* Provide different output type based on `Accept` header or url parameter. Options include:
  * `application/json` or `?json`
  * `text/html` or `?html`
  * `application/xml` or `?xml`
  * `text/plain` or `?text`
* Display IP address as viewed by the web server, or else the first entry of `HTTP_X_FORWARDED_FOR`.

## Using fifconfig
Visit the application.

    curl -L https://bgstack15.ddns.net/ifconfig/?json | jq

### Installing

You can use flask for development, and uwsgi for production.

### Instructions

Configure the application with these two files, based on the `.example` files available in the source code:

* fifconfig.conf
* fifconfig.wsgi.ini

#### Development
Run server in development mode.

    FLASK_APP=fifconfig.py FLASK_DEBUG=True flask run --host='0.0.0.0'

#### Production
Run the server in a full wsgi environment for the cleanup timer to operate.

    ./fifconfig.bin

The html responses include links to the various single-field pages, unless you add a parameter `?nolinks`. These links depend on any reverse-proxy servers adding themselves correctly to header `X-Forwarded-For`.

## Alternatives
This project was a ripoff of love of [http://ifconfig.me](http://ifconfig.me).

## References

1. [flask.Request &mdash; Flask API](https://tedboy.github.io/flask/generated/generated/flask.Request.html)
2. [my stackbin.py project](https://bgstack15.ddns.net/cgit/stackbin/tree/stackbin.py)
3. [dicttoxml - PyPI](https://pypi.org/project/dicttoxml)

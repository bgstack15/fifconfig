#!/bin/sh
# This file is part of the fifconfig package.
# It is designed to be run as root, and it will run the requisite pip commands for the discovered environment.

contents="$( awk -F'=' '/ID=|VERSION_ID/{print $2}' /etc/os-release 2>/dev/null | sed -r -e 's/"//g;' )"
id="$( echo "${contents}" | sed -n -e '1p' )"
version_id="$( echo "${contents}" | sed -n -e '2p' )"

echo "Any parameters sent to this script ${0} will be added to the list of packages to install."
piplist="${1}"

if echo "${id}" | grep -qiE 'rhel|centos' ;
then
   #if echo "${version_id}" | grep -qiE '7' ;
   #then
      piplist="${piplist} json2html"
   #fi
elif echo "${id}" | grep -qiE 'fedora' ;
then
   piplist="${piplist} json2html"
elif echo "${id}" | grep -qiE 'devuan|debian' ;
then
   piplist="${piplist} json2html"
else
   echo "Unknown os from /etc/os-release. Please investigate what pip3" 1>&2
   echo "packages are required for this OS release and share it with upstream." 1>&2
   echo "Aborted." 1>&2
   exit 1
fi

if test -n "${piplist}" ;
then
   echo "Will try to serially install with pip these packages: ${piplist}"
   for word in ${piplist} ; do
      su fifconfig -s /bin/sh -c "pip3 install --user ${word}" || exit 1
   done
fi

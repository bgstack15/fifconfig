# File: fifconfig.spec
# Location: fifconfig package
# Author: bgstack15
# SPDX-License-Identifier: CC-BY-SA-4.0
# Startdate: 2021-01-05
# Title: Rpm spec for fifconfig package
# Purpose: Provide build instructions for CentOS rpm for package
# History:
# Usage:
# Reference:
#    fuss.spec
# Improve:
# Documentation:
# Dependencies:

# Tunables
# If you set this to 1, use the systemd-rpm-macros functionality described at https://docs.fedoraproject.org/en-US/packaging-guidelines/UsersAndGroups/
%global with_systemd_usercreate 0
%global pythonver python36

# Fedora defaults
%if 0%{?fedora}
%global with_systemd_usercreate 1
%global pythonver python3
%endif

%global _appvardir %{?_localstatedir}%{!?_localstatedir:/var}/fifconfig
%global _user fifconfig

%define devtty "/dev/null"
%define debug_package %{nil}
%global _python_bytecompile_errors_terminate_build 0

Summary:    Http client diagnostic webapp
Name:       fifconfig
Version:    0.0.1
Release:    1
License:    GPL 3.0
Source0:    https://gitlab.com/bgstack15/%{name}/-/archive/master/%{name}-master.tar.gz
%if 0%{?with_systemd_usercreate}
Source1:    extra/%{name}.sysusers
%endif
#%%if ! 0%%{?fedora}
#Patch1:    extra/%%{name}-el7.patch
#%%endif
URL:        https://bgstack15.ddns.net/
Packager:   B. Stack <bgstack15@gmail.com>
Requires:   %{pythonver}-flask
Requires:   %{pythonver}-xmltodict
Requires:   uwsgi-plugin-%{pythonver}
Requires:   uwsgi-logger-file
%if 0%{?fedora}
BuildRequires: make
# Fedora needs pip helper here
#Requires:   %{pythonver}-json2html
%endif
# Mandatory pip3 requirements: pytimeparse, flask-sqlalchemy
#BuildRequires: txt2man
%if 0%{?with_systemd_usercreate}
BuildRequires: systemd-rpm-macros
%endif
%if 0%{?fedora} || 0%{?rhel} >= 8
Suggests: nginx
%endif
Buildarch:  noarch

%description
Stackbin is a flask-based pastebin implementation.

%prep
%setup -q -c %{name}
test -d "%{name}" && cd "%{name}" ; test -d "%{name}-"* && cd "%{name}-"* ;
#%%if ! 0%%{?fedora}
#%%patch1 -p1
#%%endif

%build
export srcdir="extra"
test -d "%{name}" && cd "%{name}" ; test -d "%{name}-"* && cd "%{name}-"* ;
%make_build -C "${srcdir}"

%install
export srcdir="extra"
test -d "%{name}" && cd "%{name}" ; test -d "%{name}-"* && cd "%{name}-"* ;
%make_install -C "${srcdir}" \
   with_systemd=YES \
   with_nginx=YES \
   with_apache=NO \
   with_init=NO \
%if 0%{?fedora}
   with_pip_helper=NO
%else
   with_pip_helper=YES
%endif
%if 0%{?with_systemd_usercreate}
install -p -D -m 0644 %{SOURCE1} %{buildroot}%{_sysusersdir}/%{name}.conf
%endif
exit 0

%clean
rm -rf %{buildroot}

%pre
# Reference: squid.spec
%if 0%{?with_systemd_usercreate}
%sysusers_create_compat %{SOURCE1}
%else
if ! getent group %{_user} 1>/dev/null 2>&1 ;
then
   /usr/sbin/groupadd --system --gid 725 %{_user}
fi
if ! getent passwd %{_user} 1>/dev/null 2>&1 ;
then
   /usr/sbin/useradd --system --gid 725 \
      --uid 725 --comment "fifconfig system user" \
      --home-dir %{_appvardir} --shell /sbin/nologin \
      %{_user}
fi
%endif
exit 0

%preun
%systemd_postun_with_restart %{name}.service

%post
%systemd_post %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%if 0%{?with_systemd_usercreate}
%{_sysusersdir}/%{name}.conf
%endif
%attr(0640, %{_user}, %{_user}) %config(noreplace) %{_sysconfdir}/%{name}.conf
%attr(0640, %{_user}, %{_user}) %config(noreplace) %{_sysconfdir}/%{name}.wsgi.ini
%attr(0640, %{_user}, %{_user}) %{_libexecdir}/%{name}/*.py
%attr(0755, %{_user}, %{_user}) %{_sbindir}/%{name}.bin
%attr(0750, %{_user}, %{_user}) %dir %{_appvardir}
%if ! 0%{?fedora}
%attr(0755, -, -) %{_libexecdir}/%{name}/pip-helper.sh
%endif
%attr(0644, root, root) %{?_unitdir}%{!?_unitdir:/usr/lib/systemd/system}/%{name}.service
%attr(0644, root, root) %{_sysconfdir}/nginx/default.d/%{name}.conf
%attr(0750, %{_user}, %{_user}) %dir %{?_localstatedir}%{!?_localstatedir:/var}/log/%{name}
%{_defaultdocdir}/%{name}

%changelog
* Tue Feb 15 2022 B. Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial release
